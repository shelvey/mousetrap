//
//  main.m
//  MouseTrap
//
//  Created by Sean Helvey on 3/23/13.
//  Copyright (c) 2013 Sean Helvey. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
