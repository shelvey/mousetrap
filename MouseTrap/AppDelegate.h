//
//  AppDelegate.h
//  MouseTrap
//
//  Created by Sean Helvey on 3/23/13.
//  Copyright (c) 2013 Sean Helvey. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
