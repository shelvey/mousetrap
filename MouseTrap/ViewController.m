//
//  ViewController.m
//  MouseTrap
//
//  Created by Sean Helvey on 3/23/13.
//  Copyright (c) 2013 Sean Helvey. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize imgMarble, imgMarbleView;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //Set background color
    self.view.backgroundColor = [UIColor whiteColor];
    
    imgMarbleView = [[UIImageView alloc] initWithFrame:CGRectMake(110, 110, 110, 110)];
    NSString *imgFilepath = [[NSBundle mainBundle] pathForResource:@"marble2" ofType:@"png"];
    imgMarble = [[UIImage alloc] initWithContentsOfFile:imgFilepath];
    [imgMarbleView setImage:imgMarble];
    [[self view] addSubview:imgMarbleView];
     
    // Add swipeGestures    
    UISwipeGestureRecognizer *oneFingerSwipeUp = [[UISwipeGestureRecognizer alloc]
                                                     initWithTarget:self
                                                     action:@selector(oneFingerSwipeUp:)];
    [oneFingerSwipeUp setDirection:UISwipeGestureRecognizerDirectionUp ];
    [[self view] addGestureRecognizer:oneFingerSwipeUp];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)oneFingerSwipeUp:(UITapGestureRecognizer *)recognizer {
    // Insert your own code to handle swipe right

    //GET
    NSURL* url = [NSURL URLWithString:@"http://stormy-chamber-1319.herokuapp.com/hit"];
    NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc]initWithURL:url];
    [urlRequest setHTTPMethod:@"GET"];
    
    //get response
    NSHTTPURLResponse* urlResponse = nil;
    NSError *error = [[NSError alloc] init];
    NSData *responseData = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&urlResponse error:&error];
    
    NSString* newStr = [[NSString alloc] initWithData:responseData
                                             encoding:NSUTF8StringEncoding];
    NSLog(@"%@",newStr);
    
    // Move the image
    [self moveImage:imgMarbleView duration:0.5
              curve:UIViewAnimationCurveLinear x:2.0 y:-250.0];
}

- (void)moveImage:(UIImageView *)image duration:(NSTimeInterval)duration
            curve:(int)curve x:(CGFloat)x y:(CGFloat)y
{
    // Setup the animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    // The transform matrix
    CGAffineTransform transform = CGAffineTransformMakeTranslation(x, y);
    image.transform = transform;
    
    // Commit the changes
    [UIView commitAnimations];
    
}

@end
