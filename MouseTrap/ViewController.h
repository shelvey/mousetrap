//
//  ViewController.h
//  MouseTrap
//
//  Created by Sean Helvey on 3/23/13.
//  Copyright (c) 2013 Sean Helvey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIImageView *imgMarbleView;
@property (nonatomic, retain) IBOutlet UIImage *imgMarble;

@end
